FROM python:3.8-slim
RUN pip install Django
RUN mkdir /code
WORKDIR /code
COPY . /code
CMD [ "python", "manage.py", "test"]
