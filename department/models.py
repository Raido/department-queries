from django.db import models


class Department(models.Model):

    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Department'
        verbose_name_plural = 'Departments'

class Employee(models.Model):

    department = models.ForeignKey(
        'Department',
        on_delete=models.CASCADE,
        related_name='employeers',
        null=True,
        blank=True
    )
    chief = models.ForeignKey(
        'Employee',
        on_delete=models.CASCADE,
        related_name='employeers',
        null=True,
        blank=True
    )
    name = models.CharField(max_length=255)
    salary = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'
