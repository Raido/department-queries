from django.test import TestCase
from django.db.models import Count, Max
from department.models import Department, Employee


class TestDepartmen(TestCase):

    fixtures = ["department.json"]

    def test_department_max_employeers(self):
        qs = Employee.objects.values("department_id").annotate(
            count=Count("department_id")
        )
        max_employeers_department_id = qs.latest("count")["department_id"]
        self.assertEqual(max_employeers_department_id, 3)

    def test_department_with_more_two_employeers(self):
        qs = (
            Employee.objects.values("department_id")
            .annotate(count=Count("department_id"))
            .filter(count__gt=2)
        )
        self.assertEqual(
            [i.id for i in Department.objects.filter(id__in=[1, 2, 3])],
            [i["department_id"] for i in qs],
        )

    def test_department_with_more_three_employeers(self):
        qs = (
            Employee.objects.values("department_id")
            .annotate(count=Count("department_id"))
            .filter(count__gt=3)
        )
        self.assertEqual(
            [i.id for i in Department.objects.filter(id__in=[3])],
            [i["department_id"] for i in qs],
        )
